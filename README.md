All this content was based on the [coursera course "Introduction to Software Testing"](https://www.coursera.org/learn/introduction-software-testing)

In order to implement this tests in java we are using the JUnit library. In visual studio code IDE, its important to [check this](https://code.visualstudio.com/docs/java/java-testing) to get Junit to running as a plugin.

# TDD - Test Driven development

Test driven development means that we're posed with a problem, and the first thing we do is not code the solution. But instead, create the test cases to prove that we are right when we're done. We do that so that our coding, actually building the thing, doesn't influence negatively how we go about building our tests. Because if you instead, tend to build code and then build your tests, what you also tend to do is, you write the tests to test the code rather than test the solution. So if we write our tests first, before we start coding. We can be relatively assured that our tests are going to make sure that the solution works, rather than what we coded is what we meant to code.

In order to implement test cases we should follow this steps:
- Create a class that will contain all the test cases for a specific feature;
- Then, inside that class, we will be implementing methods with an "@Test" annotation above in order to indicate the that method is an test case. Each method should indicate a unique test case an test a specific environment of execution to that feature;
- After implemented the test case, we can ran the testing class. All the test cases will get an error because the main functionality isn't implemented yet;
- now its the time to implement the functionality we want to test.
- Finally, we will run all the tests again. The main goal is to get a confirmation/ validation of the tests, meaning that the functionality just implement is working as expected.
- In case there is an error, a message like the one below will be prompt to you. In this case, the program is telling us that in a specific part of the output, it was not expecting on encounter the string "us:".
    > org.junit.ComparisonFailure: expected:[Enter the radi[] 
    > For a circle with...] but was:[Enter the radi[us:] 
    > For a circle with...]


# Dependability

Is what you would expect. We're interested in determining whether or not the software is dependable, that is whether it delivers a service such that we can rely on it.

A **service** is the system behavior as it's perceived by the user of the system. So in an airplane, the service of the airplane is to fly people from one destination to another.

A **failure** occurs when the delivered service deviates from the specification that defines what the desired service is. So we have a website where we'd like to be able to buy things and when we try and buy something, it returns an error. That would be a failure that the user can see.

An **error** is the part of the system state that can lead to a failure. So errors can be **latent** or **effective**. So we have some bugs buried in our code, and at some execution, we actually hit that bug and then the error becomes effective. Previous to that, it was latent.

**Faults** which are the root cause of the error. Now, when we're dealing with mechanical systems, it could be that a piece breaks, so you actually have a physical fault in a piece. Or that you have some error of cognition, error of understanding. So the programmer doesn't completely understand what the requirements are for the system, and so when they start writing the code, they don't do it correctly.

### How to achive dependability?

Achieving a dependable system involves utilizing four kinds of methods:
- **Fault avoidance**, which is preventing, by construction, certain kinds of faults. So if you look at different programming languages, C versus Java, for example, they have different kinds of fault avoidance. So in C, I can write an array and then I can just read past the end of it, and I can have something called a buffer overflow, that causes lots of problems with the security. Now, Java actually has as an array bounds checking. So it's not possible to write that code, that will cause the failure later on;
- **Fault tolerance**: Here, what we are going to do is we're going to have redundant subsystems such that one of them can fail and the rest will continue to operate. So you see this a lot in critical systems where you have, for example, multiple actuators that control an aileron in an airplane. So if we lose one of the actuators, if it misbehaves for some reason, we can still use the other one to move the flaps in the aircraft;
- **Error removal**: In this process, what we're trying to do is get rid of the errors themselves. So we're going to apply verification to remove latent errors;
- **Error forecasting**, which unlike the other three, is just a way of us measuring how likely we are to have failures based on looking at the behavior of the program.

#### Dependability measures

We're going to use both reliability and **availability**. So, what's the difference? So availability is the readiness of the software to respond to user requests. And **reliability** is continuity of correct service. Reliability is defined in terms of meeting requirements.

You may have an unreliable system, but if you can reboot it really fast, it actually is still pretty available. So reliability says, how long can I run this thing contiguously and have it still work correctly? And availability says, what are the chances in any given time that the system's available for me? 

**Safety** is the absence of catastrophic consequences based on failures of the software. You can have a system that's very safe, that's very unreliable. So, for example, if your car never starts, it's pretty safe as long as it's in your drive way, but it's very unreliable. On the other hand, you can have a fairly reliable system that occasionally is very unsafe. So you could have a car, that every once in awhile, exhibits unintended acceleration. Most of the time, it's reliable, but when that corner case happens, it's very unsafe.

So some other measures that are important, are **integrity**, which is the absence of improper system alteration. So when you think about security and someone taking over your computer, what you have is a failure of integrity. So they've exploited some buffer overflow, they're able to change the software, and thereby, gain access into your data.

**Maintainability**, is the ability for a process to undergo modifications and repairs. So, with software, it's always the case that you're upgrading things, and you're changing the way that the software works if it's successful in being used at scale. So this maintainability can actually contribute to reliability, because if you have to take the software offline to maintain it and it takes a long time, that's going to decrease your reliability.

#### Availability measures

So then we can turn these into numbers.
So for **reliability**, we talk about mean time between failures. So this idea of being able to run the system contiguously for a long time.
**Recoverability** is how quickly, if the system fails, it can be restored to correct operation. So this is measured in mean time to repair.
So then we can put those two numbers together, and determine **availability**. So availability is the mean time between failures divided by the mean time between failures plus the time to recover.


So one of the things that becomes important when you work in critical systems is being able to determine how robust your system is in the presence of failures like software systems lying to you, sensors and physical actuators not behaving as expected or the hardware you're running on being unreliable. When we look at the expected behavior for tests, we're going to have to set up a testing environment where we can cause some of the inputs to be unreliable.


Summarizing, dependability is how much confidence can we place in a piece of software based on the way it was developed, the testing process that we used to remove errors from it, and some mechanisms that are built into our design processes, or our language that prevents certain class of failures, excuse me. And the way that we measure dependability, we talk about it is first, we talk about faults, which are the programmers error in understanding of the situation. And then the latent errors, which are those errors in the brain turning into errors in the code. And then those turn into effective errors when we're executing the program, and we actually hit that erroneous piece of code, which can turn into failures if we don't have any fault prevention techniques in place that can respond to those errors.

When we're talking about critical systems, we have to plan for failure. We have to have software that's robust. So it's not enough that if all the inputs match our expectations, the system behaves as intended. We have to be robust to situation where the inputs don't match our expectations, and be able to respond to those situations.

#### Questions
- What is the time ordering (in terms of when a mistake is made or occurs in the running system) of the following: (1) an error, (2) a fault, and (3) a failure? 2,1,3. Faults occur potentially causing errors, which in turn may cause failures.  
- Testing helps with which kind of dependability criteria? Error removal. Failed tests lead to removing errors in the code.
- For example, if a car in a driveway does not start, it is safe, but not very robust? No, availability says the system is running, reliability says that it is doing the right thing.  
- A correct system (with respect to its requirements) will be safe? No, a correct system will be safe if the requirements are adequate to ensure safety; this is not a given.
- A correct system will be reliable? Yes. Reliability is defined in terms of meeting requirements; by definition, a correct system is reliable.
- Robust systems are reliable? No, the system may not crash (robust), but it still may not do the right thing (reliable)
- Safe systems are robust? No. For example, if a car in a driveway does not start, it is safe, but not very robust.


# Testing principles

- **What** are we trying to accomplish with testing? This is something called the Quality Process. We have certain quality goals that we want for our software. And we want to be able to achieve those quality goals as cost effectively and as rigorously as we can. Figuring out what your quality goals are and mapping them to different processes;
- **Where**, is another principle. So what we want to do is we want to focus on those areas of the code where the programmers are likely to make mistakes. It turns out that certain parts of the program and certain modules within a larger system are the ones most likely to contain bugs. And those are the ones where it makes the most sense to spend your effort testing it.
- **When**, we want to talk about testing process, when should we test? And how often should we test? And how should we schedule our testing in order to make the most effective use of our testing resources both in terms of time and money?
- **Who**, who should perform the testing? Should it be the same people who wrote the code? Should there be separate groups within an organization that perform testing, as opposed to the groups that perform development?
- **How**. When an individual tester is sitting down to write tests for a particular segment of code, what are effective strategies for focusing their attention?

## What
### Process qualities
Sometimes different quality goals can be in conflict with one another. For example making a very secure system may make it difficult to make a very usable system, those goals are often in conflict.
So what are these Quality Goals? We can break them down along a couple of different axes. The first one is goals about the process. So when we develop software, we want to have certain goals when we develop system after system. First, we want it to be **repeatable**. We don't want to get lucky once and develop software on time and on budget and then the next time completely fail. So we want to have a process that allows us to repeatedly develop our software. Second, we want one that's timely, that is low cost, that yields low defects. And over time, what we want is **continuous improvement**. So not only can we develop software as well as we did last time but we get better every time we do it.

### Product qualities
There are two different levels we can describe there as well. We have internal qualities of the product. These are things that we are concerned about as developers for the future maintenance of the system. So, we can talk about, how much reuse can we get from different pieces that we used in the current product? How easy is it to manage the software that we constructed? How maintainable is it? How easy is it to extend or modify?

We can break this down into lots of different kinds of dependability measures. **Availability**, how likely is it to be running. **Correctness**, how likely is the result that we compute likely to be the correct one. Reliability, Safety and Robustness.

## Where
So even though there are a lots of reasons for programmers to make mistakes, how those mistakes manifest themselves can be similar. And when we think about testing systematically, these are going to be the places where we want to focus our attention.
- **Floating-point numbers**: What happens is that we use floating-point numbers to model real numbers. While there are an infinite number of real numbers, and there are only a finite number of floating-point numbers. And what this means is that floating-point numbers are imprecise. people tend not to think about this. And it leads to invalid comparisons between numbers. And what's worse is when you do computations involving floating-point numbers. If you don't take specific steps to reduce imprecision, they get more and more imprecise. So over time, what is a tiny floating-point error will turn into a big floating-point error.
- **Pointers**: People tend not to think about this. And it leads to invalid comparisons between numbers. And what's worse is when you do computations involving floating-point numbers. If you don't take specific steps to reduce imprecision, they get more and more imprecise. So over time, what is a tiny floating-point error will turn into a big floating-point error.
- **Parallelism**: Suppose these two threads both access the same memory value at some point. Now, it could be most of the time this thread gets there first and then this thread gets there. But every once in a while, this thread gets there first. Just because depending on other things that are running in the processors, these different orderings occurs. And perhaps when this thing writes first, the program has a bug. So now you have something that you have to run tests over, and over again in order to try and find the one execution where this thread actually gets to that memory location first. Synchronization is something that prevents two threads from writing to the same piece of memory at the same time in java.
- **Deadlock**: Its a solution for the above point but this can also be problematic. Because maybe thread A needs to get to something that's locked by thread B, and thread B needs to get to something that's locked by thread A. And then they both get stuck, and neither one can make any progress
- **Numeric limits and boundaries**: We think the buffer should be size 32 but we only made it size 31. Or we thought we'd stop at position 99 but we actually went ahead and got into position 100. Which is kind of past the end of whatever we're trying to work with. Or we thought that this altitude should be at or above 10,000 feet. But really it should only be above 10,000 feet.
- **interrupts**: It's like your program is running along and all of a sudden it gets interrupted by the processor saying, hey wait, there's something for you to do with respect to this hardware device. And what happens is that totally disrupts the normal flow of control. Something happens, and when the program starts back up, the world has changed.
- **Complex Boolean expressions**: So if you have, if a and b or c and d and f and g and h, chances are it's very difficult to kind of figure out all the cases under which this Boolean expression is going to be true and false. And so maybe sometimes you take that if statement when you shouldn't, or vice versa.
- **Convert between types within a program**: You can lose precision, a lot of precision. And in some cases, you can overflow.

#### Questions
- Why do floating point numbers sometimes lead to erroneous code?
    - Arithmetic on floating point numbers is often approximate, so it can introduce errors;
    - Floating point numbers have values that are not actually numbers, such as infinity and NaN (not a number), that can cause computations to behave strangely;
    - As floating point computations are approximate, equality comparisons fail after computations that would succeed when using real numbers.
- Why do relational boundaries sometimes lead to erroneous code?
    - Programmers often make 'off-by-one' errors;
    - Determining strict limits on ranges is difficult in requirements engineering;
    - Relational boundaries define points of discontinuity for programs.
- Why do casts sometimes lead to erroneous code?
    - When converting an integer to a smaller bit length (e.g. long to int), the value may be truncated;
    - When converting from signed to unsigned types (e.g., int to byte), negative numbers can't be represented;
    - When converting from a double to int, the value is truncated to a whole number.

## How

So, the first thing that we want to do is we want to **divide and conquer**. So when we test, it's very difficult to test everything at once if we want to be rigorous. Instead, what we have to do is we have to look at testing at multiple levels of abstraction, so that we can be very rigorous on testing the small pieces and then we can roll up those test results in pieces in individual modules that we have confidence in towards testing the system. We want to control the scope of the tests. Another thing that we want to be able to do is divide our testing project along the purpose of tests. The third thing we're going to do is we're going to try and divide the problem in terms of testing techniques.

- **Visibility**: One of the things that we have to make sure when we write tests is that we have tests not only that test the structure of the program, but always produce things that we can observe on the outside and check;
- **Repeatability**: Flakey tests are those that pass most of the time and fail some of the time, and then someone has to take a look at them, and try to figure out what went wrong. What we would like is to write tests that always pass or always fail. Parallelism and Java sets (that are non-deterministically ordered) can be a cause of that. So one thing you have to watch out for is if your test requires resources say, from a database or a file system that the database or file system is in a consistent state each time you run the test;
- **Redundancy**;
- **Feedback**: It turns out that different applications have different pain points, different sets of modules that cause the most trouble. And once we do some initial testing, we begin to understand where those are and that's where we can focus our testing attention.

#### Questions
- Why are we often able to test more rigorously at the unit level rather than the system level?
    - The tests tend to run faster, so we can run more of them;
    - We can usually see more of the internal state at the unit level so we can build stronger oracles.
- When we say that we want redundant verification, what do we mean?
    - We want several different verification techniques checking the same program or subsystem.
- Which of the following are good ways to work with developers to reduce systematic errors?
    - Create libraries or utility functions to encapsulate operations that developers tend to get wrong;
    - Create checklists for developers based on the most common errors seen in test;
    - Create tools to test/verify specific kinds of common errors;
    - Use languages / IDEs that eliminate certain classes of errors by compile-time checks.
- We state that programs, as well as tests, can be flakey.  What does it mean for a program to be flakey?
    - Given the same inputs, sometimes the program fails a test and other times it does not.
- Why is observability an important issue in testing?
    - Often programs are stateful - that is, a test may trigger an error, but it only becomes visible as an output if a long sequence of steps are executed, whereas it might be immediately visible by examining internal state;
    - If a program error is transient (it happens and is masked out by other code), you might not be able to "see" it and the test may pass.

# V Model

The V models is a model of software development which pairs phases.
Each step along the implementation side, the implementation side of the V, should create tests and confirm that those steps are actually met along the testing side of our V model. So you'll notice that the pairs exist. For requirements there's validation, for specification there's verification. For architecture there's system integration testing, for design there's module. And for implementation or code level design there's unit testing.

- **Validation tests**: This is the set of tests that we will use to confirm that our eventual solution meets those requirements of the user. Another term you may hear is acceptance testing;
- **Verification tests**: Is the process of confirming that a software product performs and conforms to its specification. Specification or system spec is our recorded understanding of what the system will do in order to meet the user requirements. And out of specification comes Verification;
- **System integration tests**: Evaluates how various components or subsystems are going to work together. Architectural decisions can include whether or not to buy the product rather than build it;
- **Module tests**: This is essentially how you test the design module's functionality, before it's included into the larger component or subsystems;
- **Unit tests**: This is essentially how you test the design module's functionality, before it's included into the larger component or subsystems.

#### Questions
- The V-Model is: A software development model that pairs different stages of software development with the appropriate testing procedure. These tests are later used when checking the verification of each phase of the software. A testing framework is a program where test cases are defined for each part of the program. When it runs test cases, it automatically checks that the output generated conforms to the expected output. It also provides feedback on failed and successful test cases;
- The system design is tested using: We use module testing to test the design of the system;
- The specification of the system is tested using: In this step, we check that the system specification (our understanding of how the system is going to meet user requirements) is truly captured by the system;
- The difference between validation and verification is: Validation and verification address two different questions. Validation is concerned with are we building the right product while verification is concerned with are we building the product right. Indeed validation activities are planned during the user requirement phase, while verification is planned during user specification phase. Validation confirms that we are building the right product while verification confirms that we are building the product right. Validation is planned during the user requirement stage in the V-model while verification is planned during the user specification stage;
- With respect to the V-model, a system that has passed verification testing means: The system has passed all tests from unit testing through verification testing and is now ready for validation testing;
- With respect to the V-model, a valid system implies a verified system: True. A system that has passed validation testing means that it necessarily has passed the verification testing.


# Structural testing

Structural testing requires a knowledge of the internal structure of the code while black-box testing does not. In structural testing, tests are written based on the internal structure of the code - and thus white-box.

#### Questions
- what is not true about structural testing? A tester should aim for 100% structural coverage to ensure the absence of errors;
- What is not true about structural testing? The goal of testing is to achieve 100% structural coverage to ensure the absence of bugs;
- Structural testing is: White-box testing.

# Mutation testing
Mutation testing is one approach we can use to measure the adequacy of a set of tests, a way to figure out if our tests are any good bu creating several motation to the original software. Then if we run our tests and every mutant is identified, we have a pretty good test suite. If not, there is some mutant that escapes discovery by our tests and that's probably something we want to take a look at. Program testing can be used to show the presence of bugs, but never their absence.
Based on this, you can calculate a score based on how many mutants you terminate, the mutation adequacy score. In the formula below, the equivalent number represents how many mutants are essentially duplicates in terms of effect. We remove the number of duplicates when we calculate the adequacy score and we get some kind of a percentage.

#### Questions
- Which of the following is not true about mutation testing? With mutation testing, you can know how much of the code structure you covered. (True: The mutation adequacy score tells you the quality of tests; the higher the score, the better the quality of test cases. 
A mutant is killed when there exists one or more tests that can differentiate between the output of the mutant and the output of the original program);
- Which of the following are not true about mutation testing? You need to create more than one mutant.  You need anywhere from a few tens to a few hundred or more to assess the quality of your test suite.  The quality of your test suite is assessed by the number of mutants your tests kill. The mutation operator does introduce a syntactic change to the original program, but the resulting mutant must be syntactically correct and compilable in order to be used in mutation testing. A mutant is produced typically by making a single syntactic change to the original program, making them syntactically different;
- Jenny wrote a program and created 10 test cases. She also created 50 mutants from the original program to measure the adequacy of her test inputs using mutation testing. After running all of the test cases against each of the mutants, Jenny found that all of the test cases had passed on all of the mutants. In this situation, what is the most appropriate step she should take next? That all the tests have passed means that none of the tests succeed in detecting the changes (deviations from the original program, thus faults) introduced by mutation. It is likely that the ten test cases were not good enough in detecting those seeded faults.

# General questions
- Why is testing considered an 'optimistic' verification technology? The tests may all pass but the program may still be incorrect;
- If a test fails, it could mean: It is likely that the program is incorrect. It could mean that the expected answer for the test is actually wrong; tests are also software and can have bugs;
- Suppose we have two systems A and B, and A is more rigorously tested than B.  Does System A better meet its quality goals than System B? 
No;
- Unit test is used to test: (choose best answer): Implementation of the software. You write unit test to check that small chunks of the program are run correctly.
- 'Service' is the system behavior as defined by the software requirements: False;
- A latent error becomes an effective error when: The program reaches a state where the error manifests;
- Does a program terminating with an error always indicate a failure? No;
- Adaptive cruise control software that continues to run in the presence of multiple hardware and service failures but regularly misjudges the distance between cars by a substantial amount is an example of a ______ system: Certainly incorrect
- Mutation testing is a _______ metric: white-box. Mutation testing is a white-box metric: it examines the syntax of the program and makes small changes (mutations) to the code.  It does not use the requirements or program inputs in the computation of the metric, as would occur in black-box and/or grey-box metrics;
- Which of the following are true about testing? Check all that apply: It can sometimes find errors that are not actively looked for, when (for example) a program crashes during execution of a test case;
- asks that can be part of the Tear Down phase are (choose two answers): Remove data you added after testing is done (you want to clean up any data that you used during testing). Close connection after testing is done (you want to clean everything after testing is done including closing connections that you might have opened).
- In the JUnit test framework, we write test cases (choose the best answer): in a separate class, and for each method in the program we associated a test case(s) to test the correctness of the method. That is right! You need to have a separate class for testing that maps to each class in the program. The testing class will contain testing methods that test each method success and corner cases.
- Concurrent systems are quite difficult to test because (choose the best answer): The different potential interleavings of threads leads to 'race conditions' where the program may behave differently between executions. To ensure that only one thread uses a variable at a time, Java 'synchronized' code can lead to deadlocks where each thread is blocked waiting for another thread. To ensure that only one thread uses a variable at a time, Java 'synchronized' code can lead to deadlocks where each thread is blocked waiting for another thread.
- **Week 3**
- The software defect report content consists of six main components. These are the following components? Identification Information, Description of the problem, Status Indicator, Comments, Misc Information, Supporting Information;
- Which of the following components would contain a defect identification number? Identification Information;
- Which of the following components would contain an overview of the problem along with what you did and the results of that action? Description of the problem;
- Assume that you are writing a defect report and this particular defect is known to have an impact of medium severity. Which component would contain this information? Status Indicator;
- Severity and priority are essentially the same in terms of status indication. False;
- The best defect reports have new and ingenious introductions of terminology and abbreviations. False;
- Assume that there is a defect that is found to be a problem, but you can essentially ignore it. Which severity level should it have? Low;
- Lets say you are working on an unmanned aerial vehicle and a defect is found. This defect happens only occasionally, but if it does the vehicle would come crashing out of the sky onto an unsuspecting population killing many. What severity level should this defect be given? High;
- Assume that a particular low-severity defect is such that by fixing it, it will cause many more problems to come up. What final state should this defect be given? Do not fix;
- If there is a relatively easy workaround to a problem, STATE IT in the defect report. True;


# Test plan

A document describing the scope, approach, resources, and schedule of intended testing activities. It identifies test items, the feature to be tested, the testing task, who will do each task, and any risks requiring contigency planning.
Sections that sould be in your test plan:
- **Scope** means know your domain. There's no way to properly define your tests without knowing the bounds and the domain within which your project works.
- **Schedule**. I like to include the original schedule and the actual or current schedule. A statement like we will be done ten weeks after the final build is perfectly acceptable.
- **Resources**. People, materials, consumables, equipment, servers, time, testing tools, all of that's important. Who is the domain expert? Who's the technological whiz? Who's the testing guru?
- **Entry and exit criteria**. Hopefully, these come right out of our process so there is never anything new here. It just puts it in front of management and development one more time. We are not the starting testing unless, or if we find five defects, we stop. That is, if we find too many defects, we hand it back to development for further unit testing before we really start our own work.
- **Requirement matrix**.

**Test planning process**: So first, a review of the stages. **Unit testing** is where the developer tests their own code primarily for error-prone constructs and very low-level functionality assurance. **Design verification** testing covers the integration of modules through integration testing, which is done by developers, and functional testing, which is completed either by the developers or by the testing team. **System validation test** is a test-team task. Once the system has finished or nearly finished development, then the system is tested for high-level behavior and non-functional performance. And finally, **customer acceptance** involves the testing team and customer, ensuring that the final product is or will be acceptable to the customer.

Test (Status) report should include:
- Evaluate how testing went/is going;
- List what was tested;
- List what was not tested and why:
- List still-open defects (some ask for all defects);
- Show the actual schedule;
- Tell developers what works/doesn't.

[This lecture](https://www.coursera.org/learn/introduction-software-testing/lecture/vRVzZ/software-defect-reports-report-content) is the summary of the content that an software defect report should contain.

### Questions

- The unit testing plan that is generated is always a formal document: The unit testing plan is often not a formal document. It is a plan put in place by the developer while working on the project;
- Which of the following is the kind of test that determines whether or not you built the correct thing for the customer? Customer Acceptance Test. 
Customer testing is done between the developer and the customer. It ensures the final product is acceptable to the customer;
- System validation testing is when the system is tested for error prone constructs and low level functionality assurances. False. System validation testing is when the system is tested for high level behavior and nonfunctional performance. Unit testing is when the system is tested (usually by the developer) for error prone constructs and low level functionality;
- Which of the following are an important component of a test plan according to the Quality Assurance Institute? Resources and Testing Environment, Scope, Entry and Exit Conditions, Schedule. Reliability is one thing that is being tested, not a component of a test plan. The four most important components as stated in the lesson are scope, schedule, resources/testing environment, and entry/exit conditions.
- Which of the following traces test cases back to requirements? Traceability Report.
- Which of the following is true about a traceability (requirements) matrix: 
It helps find untested requirements. It helps identify test cases that don't tie to requirements. The traceability matrix determines for a set of known requirements how they are tested, it does not determine whether there are missing requirements;
- When considering concerns or risks of test planning, it's important to take preventative action when possible. True, if you know there are deficiencies in the test plan, they should be addressed.  For example, if requirements are incomplete or there is not management support for test, it is better to fix these issues early rather than late.
- It is normal to accept requirements with a 'to-be-determined' in them: False. Requirements with TBDs can't really be tested because we do not know how the system is expected to respond.
- Testing is the only way to locate bugs in a system. False. There are many other ways including proof.
- There is always pressure on ______ to prove a product is not ready for release (especially in the Waterfall Method): Testers. Testers play a somewhat adversarial role to justify their spot in the software development process.
- Unit testing is usually white box testing done by the developer on a small piece of code (a function or class): True;
- Which of these should NOT be included in unit testing? Module integration testing.Uunit testing is performed on small pieces of code, not entire modules;
- Design Verification Testing has two aspects: Integration testing and Functional testing. True. Integration testing checks whether the modules work together, and functional testing checks whether they do something useful;
- Testing modules to ensure they work together properly occurs at which stage of software testing process? Design Verification Testing;
- System validation testing occurs once the full system is complete: True;
- System validation testing will test the system for quality of behavior delivery on many aspects. Which of the following are one of those aspects? 
Usability, Portability, Scalability, Performance, Security. System validation testing is largely black-box; loop invariants are low-level things that should be unit-tested;
- Which of the following are part of the test (status) report: Open defects, What was tested, What was not tested;
- The only test report is the final report and is sent only when all testing is complete: False;
- Test (status) reports are important because: Allows management to better manage risk, Allows marketing to provide to the customers updates and manage customer expectations, Assists in process improvement through postmortem evaluations;
- Testing is _______ but not _________ from development: Independent; Isolated;
- The act of reducing risk is known as: Risk mitigation;
- The risk equation is: risk = impact * likelihood. The risk equation is risk = impact * likelihood. It is usually some dollar amount (impact) multiplied by a probability (likelihood).
- The amount of loss management is willing to accept is known as: Risk appetite. Risk appetite is the amount of loss management is willing to accept. Risk impact is the process of assessing probabilities and consequences of risk events if they are realized.
- In terms of risk impact, depth of impact refers to: Severity of damage. The depth of impact is a measure of the severity of damage as well as the availability of workarounds where breadth refers to the number of people/systems affected and the cost of the damage done.
- When considering risk, it is important to consider categories of impact. Which of the following is NOT given as an example in the lecture slides: Motivation.
- Asking the question: What can you do to control (reduce) the risk? is an example of: Risk mitigation. Risk mitigation is finding out what can be done to control and reduce risks. Risk analysis attempts to identify all the risks and quantify the severity of those risks.
- When risk avoidance fails, the best thing to do is to test features with the highest impact and the features that are most likely to fail: True. Avoid risks whenever possible, but this will not always be possible. When all else fails, risks will help prioritize.
- Only about half of testing efforts are spent on actual testing: True;
- The stages in software defect life cycles include all of the following: Analyze, report, track status, Retest bug, close bug. It seems quite unlikely that a stage in the process of handling a defect would be to ignore it. It may be the result of our analysis (e.g., we decide that though it is a bug, we won't do anything about that), but isn't a stage in every defect lifecycle;
- Part of analysis is figuring out where the fault of the defect lies. The fault could lie with the: Test, tester, product.
- Reproducibility and repeatability are the same thing when it comes to defects. False;
- Not all defects are created equal: True.
- The report phase consists of four main steps. The following are the steps: Ensure defect is not a duplicate, Talk with developers, Enter defect into the system, Make sure defect gets fixed;
- What are the characteristics of an effective defect report? Numbered or ID'd, Simple, written, Complete, Understandable;
- You should always include the minimum number of steps to reproduce the defect in a defect report: True;
- When it comes to tracking defects, the most important aspect is to have a process and follow it. True;
- Which of the following is a conclusion reached by the retesting phase? Problem fixed, Problem remains unchanged, Problem is replaced with a new problem;
- Testing and verification notes belong to which of the following categories? Close.

# Test doubles
Test doubles are lightweight versions of the components that your system under test or software under test interacts with that are necessary for us to do unit testing. 

Reasons to use it: 
- The real components may not yet be finished;
- Rhe real components are owned by someone else and they don't want you to just go out and hit them repeatedly to do your unit testing;
- Avoid side effects on real components as a result of testing;
- Sometimes we can look at the interactions that the component that we care about has with components that it talks to. So we'll call this mocking, and we'll see examples of it here in just a few slides.

Dictionary:
- **Dummy objects** which fill in dummy values into objects that are required as parameters by the system under test, but are otherwise irrelevant to the test.
- **Fake objects** are lightweight implementations of heavyweight processes like databases.
- **Mock Objects**: It's going to call out to other systems and we want to make sure that it's calling out to these other systems in a correct way. And Mock Objects are a way for us to determine whether or not our system is using other systems correctly.
- Mock Objects are fake objects that we're constructing just so that we can run the test. A **Spy Object** is something that we wrap around the real object, so that we can again, monitor these interactions between the system under test and this other object.

### Questions
- If a system under test needs to interact with other systems in order to complete a unit test, just skip the testing: False. There are strategies available to help with unit testing in an ecosystem. Don't ever skip testing because of this reason!
- What do you call lightweight versions of components that a system under test interacts with that are necessary to do unit testing? Test Doubles. Dummy objects provide test inputs for test doubles.
- What do you call the dummy values that are filled into objects that are required as parameters by the system under test, but are otherwise irrelevant? Dummy Objects;
- What are the dummy input data sources used by a system under test? Test Stubs.
- What would you use in order to check the indirect results produced by a system under test? Mock objects. Mock objects are used to check indirect results produced by a system under test. This will help show information on how the SUT uses other systems.
- There is essentially no difference between spy objects and mock objects. False. Mock objects are used to check indirect results produced by a system under test whereas spy objects wrap around a real object in order to monitor interactions between the system under test and other objects. While they are 'near' synonyms, they are not quite the same thing;
- The goal of Mockito is to: Create test doubles and inspect interactions. The goal is to create test doubles in java in order to mock away dependencies for unit tests and to inspect interactions between the system under test and mocks.
- When providing test inputs for test doubles, we want to use ____ and ____: Dummy objects; Test stubs. These would be dummy objects and test stubs. Mock and spy objects are used for outputs;
- During constructing test doubles, you wish to provide outputs for just a handful of values. What do you use? Test studs. Test stubs would provide outputs for a few values. Dummy objects provide test inputs for test doubles.
- During testing, you find that the unit tests require the use of a computationally expensive database. What could you use as a test double? Fake Objects. Test stubs would only provide outputs for a few values.
- In Mockito, there is a distiction between the roles that a class plays and how you construct it. The roles of a test double are stub, mock, and dummy (or all three). How you construct them in Mockito is the same: True. And that construction is a call to mock(Name.class).
- ______ provide the ecosystem: Test double. Test doubles provide the ecosystem. Mocks and spies will allow fine grain monitoring of the system under test.
- Mocks and spies will allow fine grain monitoring of the system under test: True. These allow you to look closely at interactions within the system (and between other systems).
- Which of the following are not a part of testing interactions using mock objects and spy objects? Required fill-in values are being created for the system under test. During the testing interaction, we wish to provide the correct parameters. Monitoring interactions includes: methods being called in proper order, methods called or not called, and proper parameters to calls are being used.
- ______ objects allow us to observe interactions of fake objects. _____ objects allow us to observe interactions of real objects: Mock; Spy. Mock objects allow observation of the interaction of fake objects whereas spy objects allow us to observe the interactions of real objects.
- In Mockito, there is no capability to override the behavior of methods with a spy object: False. You are correct; since you can wrap the method in a spy, you can also override the method's behavior.
- In Mockito, it is possible to create an object that is both a stub and a mock: True. An example of this is found in the lecture at approximately 3 minutes.

