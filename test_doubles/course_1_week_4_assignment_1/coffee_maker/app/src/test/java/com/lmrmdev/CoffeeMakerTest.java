/*
 * Copyright (c) 2009,  Sarah Heckman, Laurie Williams, Dright Ho
 * All Rights Reserved.
 * 
 * Permission has been explicitly granted to the University of Minnesota 
 * Software Engineering Center to use and distribute this source for 
 * educational purposes, including delivering online education through
 * Coursera or other entities.  
 * 
 * No warranty is given regarding this software, including warranties as
 * to the correctness or completeness of this software, including 
 * fitness for purpose.
 * 
 * 
 * Modifications 
 * 20171114 - Ian De Silva - Updated to comply with JUnit 4 and to adhere to 
 * 							 coding standards.  Added test documentation.
 */
package com.lmrmdev;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.lmrmdev.exceptions.InventoryException;
import com.lmrmdev.exceptions.RecipeException;

/**
 * Unit tests for CoffeeMaker class.
 * 
 * @author Sarah Heckman
 */
public class CoffeeMakerTest {
	
	/**
	 * The object under test.
	 */
	private CoffeeMaker coffeeMaker;
	
	// Sample recipes to use in testing.
	private Recipe recipe1;
	private Recipe recipe2;
	private Recipe recipe3;
	private Recipe recipe4;

	/**
	 * Initializes some recipes to test with and the {@link CoffeeMaker} 
	 * object we wish to test.
	 * 
	 * @throws RecipeException  if there was an error parsing the ingredient 
	 * 		amount when setting up the recipe.
	 */
	@Before
	public void setUp() throws RecipeException {
		coffeeMaker = new CoffeeMaker();
		
		//Set up for r1
		recipe1 = new Recipe();
		recipe1.setName("Coffee");
		recipe1.setAmtChocolate("0");
		recipe1.setAmtCoffee("3");
		recipe1.setAmtMilk("1");
		recipe1.setAmtSugar("1");
		recipe1.setPrice("50");
		
		//Set up for r2
		recipe2 = new Recipe();
		recipe2.setName("Mocha");
		recipe2.setAmtChocolate("20");
		recipe2.setAmtCoffee("3");
		recipe2.setAmtMilk("1");
		recipe2.setAmtSugar("1");
		recipe2.setPrice("75");
		
		//Set up for r3
		recipe3 = new Recipe();
		recipe3.setName("Latte");
		recipe3.setAmtChocolate("0");
		recipe3.setAmtCoffee("3");
		recipe3.setAmtMilk("3");
		recipe3.setAmtSugar("1");
		recipe3.setPrice("100");
		
		//Set up for r4
		recipe4 = new Recipe();
		recipe4.setName("Hot Chocolate");
		recipe4.setAmtChocolate("4");
		recipe4.setAmtCoffee("0");
		recipe4.setAmtMilk("1");
		recipe4.setAmtSugar("1");
		recipe4.setPrice("65");

	}
	
	
	/**
	 * Given a coffee maker with the default inventory
	 * When we add inventory with well-formed quantities
	 * Then we do not get an exception trying to read the inventory quantities.
	 * 
	 * @throws InventoryException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test
	public void testAddInventory() throws InventoryException {
		coffeeMaker.addInventory("4","7","0","9");
	}
	
	/**
	 * Given a coffee maker with the default inventory
	 * When we add inventory with malformed quantities (i.e., a negative 
	 * quantity and a non-numeric string)
	 * Then we get an inventory exception
	 * 
	 * @throws InventoryException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventoryException() throws InventoryException {
		coffeeMaker.addInventory("4", "-1", "asdf", "3");
	}
	
	/**
	 * Given a coffee maker with one valid recipe
	 * When we make coffee, selecting the valid recipe and paying more than 
	 * 		the coffee costs
	 * Then we get the correct change back.
	 */
	@Test
	public void testMakeCoffee() {
		coffeeMaker.addRecipe(recipe1);
		assertEquals(25, coffeeMaker.makeCoffee(0, 75));
	}

	/**
	 * Given a coffee maker with the default recipe
	 * When we add recipe with malformed quantities (i.e., a negative 
	 * quantity, a non-numeric string, null or empty)
	 * Then we get an recipe exception
	 * 
	 * @throws RecipeException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeName() throws RecipeException {
		// r5_1: Only the name is null
		Recipe recipe5_1 = new Recipe();
		recipe5_1.setName(null);
	}
	*/

	/**
	 * Given a coffee maker with the default recipe
	 * When we add recipe with malformed quantities (i.e., a negative 
	 * quantity, a non-numeric string, null or empty)
	 * Then we get an recipe exception
	 * 
	 * @throws RecipeException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeAmtChocolate() throws RecipeException {
		// r5_2: Only the amount of chocolate is null
		Recipe recipe5_2 = new Recipe();
		recipe5_2.setAmtChocolate(null);
	}

		/**
	 * Given a coffee maker with the default recipe
	 * When we add recipe with malformed quantities (i.e., a negative 
	 * quantity, a non-numeric string, null or empty)
	 * Then we get an recipe exception
	 * 
	 * @throws RecipeException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeAmtCoffee() throws RecipeException {
		// r5_3: Only the amount of coffee is null
		Recipe recipe5_3 = new Recipe();
		recipe5_3.setAmtCoffee(null);
	}		
	
	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
   @Test(expected = RecipeException.class)
   public void testAddRecipe_EmptyRecipeAmtMilk() throws RecipeException {
		// r5_4: Only the amount of milk is null
		Recipe recipe5_4 = new Recipe();
		recipe5_4.setAmtMilk(null);
   }

   /**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeAmtSugar() throws RecipeException {
		// r5_5: Only the amount of sugar is null
		Recipe recipe5_5 = new Recipe();
		recipe5_5.setAmtSugar(null);
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipePrice() throws RecipeException {
		// r5_6: Only the price is null
		Recipe recipe5_6 = new Recipe();
		recipe5_6.setPrice(null);
	}


	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeName() throws RecipeException {
		// r6_1: Only the name property is empty
		Recipe recipe6_1 = new Recipe();
		recipe6_1.setName("");
	}
	*/

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtChocolate() throws RecipeException {
		// r6_2: Only the amount of chocolate property is empty
		Recipe recipe6_2 = new Recipe();
		recipe6_2.setAmtChocolate("");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtCoffee() throws RecipeException {
		// r6_3: Only the amount of coffee property is empty
		Recipe recipe6_3 = new Recipe();
		recipe6_3.setAmtCoffee("");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtMilk() throws RecipeException {
		// r6_4: Only the amount of milk property is empty
		Recipe recipe6_4 = new Recipe();
		recipe6_4.setAmtMilk("");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtSugar() throws RecipeException {
		// r6_5: Only the amount of sugar property is empty
		Recipe recipe6_5 = new Recipe();
		recipe6_5.setAmtSugar("");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipePrice() throws RecipeException {
		// r6_5: Only the price property is empty
		Recipe recipe6_6 = new Recipe();
		recipe6_6.setPrice("");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtChocolate() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of chocolate
		Recipe recipe7_1 = new Recipe();
		recipe7_1.setAmtChocolate("-1");		
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtCoffee() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of coffee
		Recipe recipe7_2 = new Recipe();
		recipe7_2.setAmtCoffee("-1");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtMilk() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of milk
		Recipe recipe7_3 = new Recipe();
		recipe7_3.setAmtMilk("-1");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtSugar() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of sugar
		Recipe recipe7_4 = new Recipe();
		recipe7_4.setAmtSugar("-1");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipePrice() throws RecipeException {
		// r7_1: Numeric string with negative values: price
		Recipe recipe7_5 = new Recipe();
		recipe7_5.setPrice("-1");
	}







/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtChocolate() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of chocolate
		Recipe recipe8_1 = new Recipe();
		recipe8_1.setAmtChocolate("qwrw");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtCoffee() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of coffee
		Recipe recipe8_2 = new Recipe();
		recipe8_2.setAmtCoffee("qwer");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtMilk() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of milk
		Recipe recipe8_3 = new Recipe();
		recipe8_3.setAmtMilk("qwer");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtSugar() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of sugar
		Recipe recipe8_4 = new Recipe();
		recipe8_4.setAmtSugar("qwer");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipePrice() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Price
		Recipe recipe8_5 = new Recipe();
		recipe8_5.setPrice("qwer");
	}


	/**
	* Given a coffee maker with the default recipe
	* When we add a recipe with a name that is 
	* already in the databse 
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error trying to add a 
	*	non unique recipe to the database.
	*/
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonUniqueRecipie() throws RecipeException {
		coffeeMaker.addRecipe(recipe1);
		coffeeMaker.addRecipe(recipe1);
	}
	*/

	/**
	* The max number of recipies in the database cannot pass over 3.
	* When this occours, a recipie exception must be thrown.
	* 
	* @throws RecipeException  if there were already 3 unique recipies in the 
	*	database.
	*/
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_MoreThanThreeUniqueRecipiesInTheDatabase() throws RecipeException {
		coffeeMaker.addRecipe(recipe1);
		coffeeMaker.addRecipe(recipe2);
		coffeeMaker.addRecipe(recipe3);

		// This one will cause the exception to be thrown
		coffeeMaker.addRecipe(recipe4);
	}
	*/






}
