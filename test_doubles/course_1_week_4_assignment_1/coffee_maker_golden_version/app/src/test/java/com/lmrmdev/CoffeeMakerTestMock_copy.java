/*
 * Copyright (c) 2009,  Sarah Heckman, Laurie Williams, Dright Ho
 * All Rights Reserved.
 * 
 * Permission has been explicitly granted to the University of Minnesota 
 * Software Engineering Center to use and distribute this source for 
 * educational purposes, including delivering online education through
 * Coursera or other entities.  
 * 
 * No warranty is given regarding this software, including warranties as
 * to the correctness or completeness of this software, including 
 * fitness for purpose.
 * 
 * Modifications
 * 20171113 - Michael W. Whalen - Extended with additional recipe.
 * 20171114 - Ian J. De Silva   - Updated to JUnit 4; fixed variable names.
 */

package edu.ncsu.csc326.coffeemaker;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import edu.ncsu.csc326.coffeemaker.exceptions.RecipeException;

/**
 * Unit tests for CoffeeMaker class.
 * 
 * @author Sarah Heckman
 *
 * Extended by Mike Whalen
 */

public class CoffeeMakerTest {
    	
	//-----------------------------------------------------------------------
	//	DATA MEMBERS
	//-----------------------------------------------------------------------
	private Recipe recipe1;
	private Recipe recipe2;
	private Recipe recipe3;
	private Recipe recipe4;
	private Recipe recipe5;
    private Recipe recipe6;
	
	private Recipe [] stubRecipies; 
	
	/**
	 * The coffee maker -- our object under test.
	 */
	private CoffeeMaker coffeeMaker;
	
	/**
	 * The stubbed recipe book.
	 */
	private RecipeBook recipeBookStub;
	
	
	//-----------------------------------------------------------------------
	//	Set-up / Tear-down
	//-----------------------------------------------------------------------
	/**
	 * Initializes some recipes to test with, creates the {@link CoffeeMaker} 
	 * object we wish to test, and stubs the {@link RecipeBook}. 
	 * 
	 * @throws RecipeException  if there was an error parsing the ingredient 
	 * 		amount when setting up the recipe.
	 */
	@Before
	public void setUp() throws RecipeException {
		
		recipeBookStub = mock(RecipeBook.class);
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
		
		//Set up for recipe1
		recipe1 = new Recipe();
		recipe1.setName("Coffee");
		recipe1.setAmtChocolate("0");
		recipe1.setAmtCoffee("3");
		recipe1.setAmtMilk("1");
		recipe1.setAmtSugar("1");
		recipe1.setPrice("50");
		
		//Set up for recipe2
		recipe2 = new Recipe();
		recipe2.setName("Mocha");
		recipe2.setAmtChocolate("20");
		recipe2.setAmtCoffee("3");
		recipe2.setAmtMilk("1");
		recipe2.setAmtSugar("1");
		recipe2.setPrice("75");
		
		//Set up for recipe3
		recipe3 = new Recipe();
		recipe3.setName("Latte");
		recipe3.setAmtChocolate("0");
		recipe3.setAmtCoffee("3");
		recipe3.setAmtMilk("3");
		recipe3.setAmtSugar("1");
		recipe3.setPrice("100");
		
		//Set up for recipe4
		recipe4 = new Recipe();
		recipe4.setName("Hot Chocolate");
		recipe4.setAmtChocolate("4");
		recipe4.setAmtCoffee("0");
		recipe4.setAmtMilk("1");
		recipe4.setAmtSugar("1");
		recipe4.setPrice("65");
		
		//Set up for recipe5 (added by MWW)
		recipe5 = new Recipe();
		recipe5.setName("Super Hot Chocolate");
		recipe5.setAmtChocolate("6");
		recipe5.setAmtCoffee("60");
		recipe5.setAmtMilk("1");
		recipe5.setAmtSugar("1");
		recipe5.setPrice("100");

		//Set up for recipe6 (added by MWW)
		recipe6 = new Recipe();
		recipe6.setName("Super Hot Chocolate");
		recipe6.setAmtChocolate("6");
		recipe6.setAmtCoffee("0");
		recipe6.setAmtMilk("1");
		recipe6.setAmtSugar("40");
		recipe6.setPrice("100");

		stubRecipies = new Recipe [] {recipe1, recipe2, recipe3};
	}
	
	@Test
	public void testMakeCoffee() {
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
		assertTrue(true);
	}

	@Test
	public void test_get_drink_not_existent_recipe(){
		// Arrange
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1, recipe6, null});

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
		int actual = coffeeMaker.makeCoffee(2, 100);

        // Assert
		assertEquals(100, actual);
		
	}

	@Test
	public void test_get_drink_not_existent_recipe_2(){
		// Arrange
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1, recipe6, null});

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
		int actual = coffeeMaker.makeCoffee(4, 100);
        
        // Assert
		assertEquals(100, actual);
		
	}

	@Test
	public void test_get_drink_not_enough_ingredients_2(){
		// Create RecipeBook stub
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1, recipe5, null});

		// Create coffee maker
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
        int actual = coffeeMaker.makeCoffee(1, 100);

		// Make coffee
		assertEquals(100, actual);
		
	}

	@Test
	public void test_get_drink_test_enoughIngredients(){
		// Arrange
		Inventory inventorySpy= spy(new Inventory());
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1, recipe6, recipe3});

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, inventorySpy);
		coffeeMaker.makeCoffee(2, 100);

		// Assert/ Verify
		verify(inventorySpy).enoughIngredients(recipe3);
		
	}

    @Test
	public void test_check_if_method_was_called_2(){
		// Arrange
		Recipe recipe1Spy= spy(recipe1);
		Recipe recipe6Spy= spy(recipe6);
		Recipe recipe3Spy= spy(recipe3);
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1Spy, recipe6Spy, recipe3Spy});

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
		int actual = coffeeMaker.makeCoffee(1, 100);
		
        // Assert
        assertEquals(100, actual);

		verify(recipe3Spy, times(0)).getAmtChocolate();
		verify(recipe3Spy, times(0)).getAmtMilk();
		verify(recipe3Spy, times(0)).getAmtSugar();
		verify(recipe3Spy, times(0)).getAmtCoffee();
		verify(recipe3Spy, times(0)).getPrice();

        verify(recipe6Spy, times(1)).getAmtChocolate();
		verify(recipe6Spy, times(1)).getAmtMilk();
		verify(recipe6Spy, times(1)).getAmtSugar();
		verify(recipe6Spy, times(1)).getAmtCoffee();
		verify(recipe6Spy, times(1)).getPrice();

        verify(recipe1Spy, times(0)).getAmtChocolate();
		verify(recipe1Spy, times(0)).getAmtMilk();
		verify(recipe1Spy, times(0)).getAmtSugar();
		verify(recipe1Spy, times(0)).getAmtCoffee();
		verify(recipe1Spy, times(0)).getPrice();
	}


	@Test
	public void test_check_if_method_was_called(){
		// Arrange
		Recipe recipe1Spy= spy(recipe1);
		Recipe recipe6Spy= spy(recipe6);
		Recipe recipe3Spy= spy(recipe3);
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1Spy, recipe6Spy, recipe3Spy});

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
		int actual = coffeeMaker.makeCoffee(2, 100);
        
        // Assert
		assertEquals(0, actual);
		verify(recipe3Spy, atLeastOnce()).getAmtChocolate();
		verify(recipe3Spy, atLeastOnce()).getAmtMilk();
		verify(recipe3Spy, atLeastOnce()).getAmtSugar();
		verify(recipe3Spy, atLeastOnce()).getAmtCoffee();
		verify(recipe3Spy, atLeastOnce()).getPrice();

		// Assert other recipes
        verify(recipe6Spy, times(0)).getAmtChocolate();
		verify(recipe6Spy, times(0)).getAmtMilk();
		verify(recipe6Spy, times(0)).getAmtSugar();
		verify(recipe6Spy, times(0)).getAmtCoffee();
		verify(recipe6Spy, times(0)).getPrice();

        // Other one
		verify(recipe1Spy, times(0)).getAmtChocolate();
		verify(recipe1Spy, times(0)).getAmtMilk();
		verify(recipe1Spy, times(0)).getAmtSugar();
		verify(recipe1Spy, times(0)).getAmtCoffee();
		verify(recipe1Spy, times(0)).getPrice();
	}

	@Test
	public void test_check_if_method_was_called_3(){
		// Arrange
		Recipe recipe1Spy= spy(recipe1);
		Recipe recipe6Spy= spy(recipe6);
		Recipe recipe3Spy= spy(recipe3);
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1Spy, recipe6Spy, recipe3Spy});

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
        int actual = coffeeMaker.makeCoffee(0, 100);

		// Assert
		assertEquals(50, actual);

		verify(recipe6Spy, times(0)).getAmtChocolate();
		verify(recipe6Spy, times(0)).getAmtMilk();
		verify(recipe6Spy, times(0)).getAmtSugar();
		verify(recipe6Spy, times(0)).getAmtCoffee();
		verify(recipe6Spy, times(0)).getPrice();

        verify(recipe1Spy, atLeastOnce()).getAmtChocolate();
		verify(recipe1Spy, atLeastOnce()).getAmtMilk();
		verify(recipe1Spy, atLeastOnce()).getAmtSugar();
		verify(recipe1Spy, atLeastOnce()).getAmtCoffee();
		verify(recipe1Spy, atLeastOnce()).getPrice();

        verify(recipe3Spy, times(0)).getAmtChocolate();
		verify(recipe3Spy, times(0)).getAmtMilk();
		verify(recipe3Spy, times(0)).getAmtSugar();
		verify(recipe3Spy, times(0)).getAmtCoffee();
		verify(recipe3Spy, times(0)).getPrice();

	}

	@Test
	public void test_inventory_decrease(){
		// Arrange
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1, recipe6, null});

		// Act
		Inventory inventory= new Inventory();
		coffeeMaker = new CoffeeMaker(recipeBookStub, inventory);
		String prevInventory= coffeeMaker.checkInventory();
		coffeeMaker.makeCoffee(0, 100);

		// Assert
		assertNotEquals(prevInventory, coffeeMaker.checkInventory());
		
	}

	@Test
	public void test_inventory_decrease_2(){
		// Arrange
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1, recipe6, null});

		// Act
		Inventory inventory= new Inventory();
		coffeeMaker = new CoffeeMaker(recipeBookStub, inventory);
		coffeeMaker.makeCoffee(0, 100);

		// Assert
		assertEquals(15-recipe1.getAmtChocolate(), inventory.getChocolate());
		assertEquals(15-recipe1.getAmtCoffee(), inventory.getCoffee());
        assertEquals(15-recipe1.getAmtMilk(), inventory.getMilk());
		assertEquals(15-recipe1.getAmtSugar(), inventory.getSugar());
		
	}



	@Test
	public void test_get_drink_not_enough_ingredients(){
		// Arrange
		when(recipeBookStub.getRecipes()).thenReturn(new Recipe[]{recipe1, recipe6, null});

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
        int actual = coffeeMaker.makeCoffee(1, 100);
		
        // Assert
		assertEquals(100, actual);
		
	}

    @Test
	public void test_get_drink_right_money(){
        // arrange
		when(recipeBookStub.getRecipes()).thenReturn(stubRecipies);

		// act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
		
        // assert
		assertEquals(50, coffeeMaker.makeCoffee(0, 100));
		
	}


	@Test
	public void test_get_drink_less_money(){
		// Arrange
		when(recipeBookStub.getRecipes()).thenReturn(stubRecipies);

		// Act
		coffeeMaker = new CoffeeMaker(recipeBookStub, new Inventory());
        int actual = coffeeMaker.makeCoffee(0, 5);
		
        // assert
		assertEquals(5, actual);
		
	}

}

