/*
 * Copyright (c) 2009,  Sarah Heckman, Laurie Williams, Dright Ho
 * All Rights Reserved.
 * 
 * Permission has been explicitly granted to the University of Minnesota 
 * Software Engineering Center to use and distribute this source for 
 * educational purposes, including delivering online education through
 * Coursera or other entities.  
 * 
 * No warranty is given regarding this software, including warranties as
 * to the correctness or completeness of this software, including 
 * fitness for purpose.
 * 
 * 
 * Modifications 
 * 20171114 - Ian De Silva - Updated to comply with JUnit 4 and to adhere to 
 * 							 coding standards.  Added test documentation.
 */

// This implementation was based on: https://github.com/Faaizz/introduction_to_software_testing
// How to create an gradle project: https://www.youtube.com/watch?v=4cT30wRJXVc

package com.lmrmdev;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.lmrmdev.exceptions.InventoryException;
import com.lmrmdev.exceptions.RecipeException;

/**
 * Unit tests for CoffeeMaker class.
 * 
 * @author Sarah Heckman
 */
public class CoffeeMakerTest {
	
	/**
	 * The object under test.
	 */
	private CoffeeMaker coffeeMaker;
	
	// Sample recipes to use in testing.
	private Recipe recipe1;
	private Recipe recipe2;
	private Recipe recipe3;
	private Recipe recipe4;

	/**
	 * Initializes some recipes to test with and the {@link CoffeeMaker} 
	 * object we wish to test.
	 * 
	 * @throws RecipeException  if there was an error parsing the ingredient 
	 * 		amount when setting up the recipe.
	 */
	@Before
	public void setUp() throws RecipeException {
		coffeeMaker = new CoffeeMaker();
		
		//Set up for r1
		recipe1 = new Recipe();
		recipe1.setName("Coffee");
		recipe1.setAmtChocolate("0");
		recipe1.setAmtCoffee("3");
		recipe1.setAmtMilk("1");
		recipe1.setAmtSugar("1");
		recipe1.setPrice("50");
		
		//Set up for r2
		recipe2 = new Recipe();
		recipe2.setName("Mocha");
		recipe2.setAmtChocolate("20");
		recipe2.setAmtCoffee("3");
		recipe2.setAmtMilk("1");
		recipe2.setAmtSugar("1");
		recipe2.setPrice("75");
		
		//Set up for r3
		recipe3 = new Recipe();
		recipe3.setName("Latte");
		recipe3.setAmtChocolate("0");
		recipe3.setAmtCoffee("3");
		recipe3.setAmtMilk("3");
		recipe3.setAmtSugar("1");
		recipe3.setPrice("100");
		
		//Set up for r4
		recipe4 = new Recipe();
		recipe4.setName("Hot Chocolate");
		recipe4.setAmtChocolate("4");
		recipe4.setAmtCoffee("0");
		recipe4.setAmtMilk("1");
		recipe4.setAmtSugar("1");
		recipe4.setPrice("65");

	}
	
	
	/**
	 * Given a coffee maker with the default inventory
	 * When we add inventory with well-formed quantities
	 * Then we do not get an exception trying to read the inventory quantities.
	 * 
	 * @throws InventoryException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test
	public void testAddInventory() throws InventoryException {
		coffeeMaker.addInventory("4","7","0","9");
	}
	
	/**
	 * Given a coffee maker with the default inventory
	 * When we add inventory with malformed quantities (i.e., a negative 
	 * quantity and a non-numeric string)
	 * Then we get an inventory exception
	 * 
	 * @throws InventoryException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventoryException() throws InventoryException {
		coffeeMaker.addInventory("4", "-1", "asdf", "3");
	}
	
	/**
	 * Given a coffee maker with one valid recipe
	 * When we make coffee, selecting the valid recipe and paying more than 
	 * 		the coffee costs
	 * Then we get the correct change back.
	 */
	@Test
	public void testMakeCoffee() {
		coffeeMaker.addRecipe(recipe1);
		assertEquals(25, coffeeMaker.makeCoffee(0, 75));
	}

	/**
	 * Given a coffee maker with the default recipe
	 * When we add recipe with malformed quantities (i.e., a negative 
	 * quantity, a non-numeric string, null or empty)
	 * Then we get an recipe exception
	 * 
	 * @throws RecipeException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeName() throws RecipeException {
		// r5_1: Only the name is null
		Recipe recipe5_1 = new Recipe();
		recipe5_1.setName(null);
	}
	*/

	/**
	 * Given a coffee maker with the default recipe
	 * When we add recipe with malformed quantities (i.e., a negative 
	 * quantity, a non-numeric string, null or empty)
	 * Then we get an recipe exception
	 * 
	 * @throws RecipeException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeAmtChocolate() throws RecipeException {
		// r5_2: Only the amount of chocolate is null
		Recipe recipe5_2 = new Recipe();
		recipe5_2.setAmtChocolate(null);
	}

		/**
	 * Given a coffee maker with the default recipe
	 * When we add recipe with malformed quantities (i.e., a negative 
	 * quantity, a non-numeric string, null or empty)
	 * Then we get an recipe exception
	 * 
	 * @throws RecipeException  if there was an error parsing the quanity
	 * 		to a positive integer.
	 */
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeAmtCoffee() throws RecipeException {
		// r5_3: Only the amount of coffee is null
		Recipe recipe5_3 = new Recipe();
		recipe5_3.setAmtCoffee(null);
	}		
	
	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
   @Test(expected = RecipeException.class)
   public void testAddRecipe_EmptyRecipeAmtMilk() throws RecipeException {
		// r5_4: Only the amount of milk is null
		Recipe recipe5_4 = new Recipe();
		recipe5_4.setAmtMilk(null);
   }

   /**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipeAmtSugar() throws RecipeException {
		// r5_5: Only the amount of sugar is null
		Recipe recipe5_5 = new Recipe();
		recipe5_5.setAmtSugar(null);
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_EmptyRecipePrice() throws RecipeException {
		// r5_6: Only the price is null
		Recipe recipe5_6 = new Recipe();
		recipe5_6.setPrice(null);
	}


	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeName() throws RecipeException {
		// r6_1: Only the name property is empty
		Recipe recipe6_1 = new Recipe();
		recipe6_1.setName("");
	}
	*/

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtChocolate() throws RecipeException {
		// r6_2: Only the amount of chocolate property is empty
		Recipe recipe6_2 = new Recipe();
		recipe6_2.setAmtChocolate("");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtCoffee() throws RecipeException {
		// r6_3: Only the amount of coffee property is empty
		Recipe recipe6_3 = new Recipe();
		recipe6_3.setAmtCoffee("");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtMilk() throws RecipeException {
		// r6_4: Only the amount of milk property is empty
		Recipe recipe6_4 = new Recipe();
		recipe6_4.setAmtMilk("");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipeAmtSugar() throws RecipeException {
		// r6_5: Only the amount of sugar property is empty
		Recipe recipe6_5 = new Recipe();
		recipe6_5.setAmtSugar("");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NoValueRecipePrice() throws RecipeException {
		// r6_5: Only the price property is empty
		Recipe recipe6_6 = new Recipe();
		recipe6_6.setPrice("");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtChocolate() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of chocolate
		Recipe recipe7_1 = new Recipe();
		recipe7_1.setAmtChocolate("-1");		
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtCoffee() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of coffee
		Recipe recipe7_2 = new Recipe();
		recipe7_2.setAmtCoffee("-1");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtMilk() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of milk
		Recipe recipe7_3 = new Recipe();
		recipe7_3.setAmtMilk("-1");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipeAmtSugar() throws RecipeException {
		// r7_1: Numeric string with negative values: Amout of sugar
		Recipe recipe7_4 = new Recipe();
		recipe7_4.setAmtSugar("-1");	
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NegativeValueRecipePrice() throws RecipeException {
		// r7_1: Numeric string with negative values: price
		Recipe recipe7_5 = new Recipe();
		recipe7_5.setPrice("-1");
	}







/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtChocolate() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of chocolate
		Recipe recipe8_1 = new Recipe();
		recipe8_1.setAmtChocolate("qwrw");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtCoffee() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of coffee
		Recipe recipe8_2 = new Recipe();
		recipe8_2.setAmtCoffee("qwer");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtMilk() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of milk
		Recipe recipe8_3 = new Recipe();
		recipe8_3.setAmtMilk("qwer");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipeAmtSugar() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Ammount of sugar
		Recipe recipe8_4 = new Recipe();
		recipe8_4.setAmtSugar("qwer");
	}

	/**
	* Given a coffee maker with the default recipe
	* When we add recipe with malformed quantities (i.e., a negative 
	* quantity, a non-numeric string, null or empty)
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error parsing the quanity
	* 		to a positive integer.
	*/
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonNumericValueRecipePrice() throws RecipeException {
		// r8_1: Non numeric values in numeric string property: Price
		Recipe recipe8_5 = new Recipe();
		recipe8_5.setPrice("qwer");
	}


	/**
	* Given a coffee maker with the default recipe
	* When we add a recipe with a name that is 
	* already in the databse 
	* Then we get an recipe exception
	* 
	* @throws RecipeException  if there was an error trying to add a 
	*	non unique recipe to the database.
	*/
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_NonUniqueRecipie() throws RecipeException {
		coffeeMaker.addRecipe(recipe1);
		coffeeMaker.addRecipe(recipe1);
	}
	*/

	/**
	* The max number of recipies in the database cannot pass over 3.
	* When this occours, a recipie exception must be thrown.
	* 
	* @throws RecipeException  if there were already 3 unique recipies in the 
	*	database.
	*/
	/*
	@Test(expected = RecipeException.class)
	public void testAddRecipe_MoreThanThreeUniqueRecipiesInTheDatabase() throws RecipeException {
		coffeeMaker.addRecipe(recipe1);
		coffeeMaker.addRecipe(recipe2);
		coffeeMaker.addRecipe(recipe3);

		// This one will cause the exception to be thrown
		coffeeMaker.addRecipe(recipe4);
	}
	*/

	/**
	 * Given a coffee maker with the default inventory
	 * When we add a recipe to the book, the return value of the addRecipe 
	 * function must be true
	 * 
	 */
	@Test
	public void testAddRecipe_ValidRecipe() {
		boolean result = coffeeMaker.addRecipe(recipe1);
		assertTrue(result);
	}

/**
	 * Given a cfee maker with no recipes
	 * When we add 3 valid recipesof
	 * Then we get a coffee maker with 3 recipes.
	 */
	@Test
	public void testAddRecipe_AddedRecipiesAreInTheDatabase() {
		boolean added_1 = coffeeMaker.addRecipe(recipe1); 
		assertTrue(added_1);
		boolean added_2 = coffeeMaker.addRecipe(recipe2); 
		assertTrue(added_2);
		boolean added_3 = coffeeMaker.addRecipe(recipe3); 
		assertTrue(added_3);
		assertNotEquals(null, coffeeMaker.getRecipes()[0]);
		assertNotEquals(null, coffeeMaker.getRecipes()[1]);
		assertNotEquals(null, coffeeMaker.getRecipes()[2]);
	}

	/*
	 * Given a cfee maker with no recipes
	 * When we add 4 valid recipes
	 * The fourth one has to return false 
	 * and not be present in the recipies book
	 */
	@Test
	public void testAddRecipe_Only3CanBeAdded() {
		boolean added_1 = coffeeMaker.addRecipe(recipe1); 
		assertTrue(added_1);
		boolean added_2 = coffeeMaker.addRecipe(recipe2); 
		assertTrue(added_2);
		boolean added_3 = coffeeMaker.addRecipe(recipe3); 
		assertTrue(added_3);
		boolean added_4 = coffeeMaker.addRecipe(recipe4); 
		assertFalse(added_4);
	}

/**
	 * Given a coffee maker with no recipes
	 * When we add a recipe with the same name as an existing one
	 * Then we get a false response.
	 */
	@Test
	public void testAddRecipe_RecipeWithUniqueNames() {
		recipe2.setName(recipe1.getName());
		boolean added_1 = coffeeMaker.addRecipe(recipe1); 
		assertTrue(added_1);
		boolean added_2 = coffeeMaker.addRecipe(recipe2); 
		assertFalse(added_2);
	}


	/**
	 * Given a coffee maker with no recipes
	 * When we add a valid recipe and then edit the recipe
	 * Then we get a coffee maker with a recipe corresponding to the new data.
	 */
	@Test
	public void testAddRecipe_ValuesAfterEdit() {
		coffeeMaker.addRecipe(recipe1);
		coffeeMaker.editRecipe(0, recipe2);
		assertEquals(recipe2.getPrice(), coffeeMaker.getRecipes()[0].getPrice());
		assertEquals(recipe2.getAmtChocolate(), coffeeMaker.getRecipes()[0].getAmtChocolate());
		assertEquals(recipe2.getAmtCoffee(), coffeeMaker.getRecipes()[0].getAmtCoffee());
		assertEquals(recipe2.getAmtMilk(), coffeeMaker.getRecipes()[0].getAmtMilk());
		assertEquals(recipe2.getAmtSugar(), coffeeMaker.getRecipes()[0].getAmtSugar());
	}

	/**
	 * Given a coffee maker with the default inventory
	 * When we try to delete a recipe to the book when 
	 * there is not any recipe in it, the return value 
	 * of the deleteRecipe method must be null
	 * 
	 */
	@Test
	public void testDeleteRecipe_NonExistentRecipe_1() {
		String result = coffeeMaker.deleteRecipe(1);
		assertNull(result);
	}

	/**
	 * Given a coffee maker with the default inventory
	 * When we try to delete the first recipe in the book, when 
	 * there is one recipe in it, the return value 
	 * of the deleteRecipe method must be not null
	 * 
	 */
	@Test
	public void testDeleteRecipe_ValidExistentRecipe() {
		coffeeMaker.addRecipe(recipe1); // Add recipe to database. In hte future, this recipe can be accessed with the number 1
		String result = coffeeMaker.deleteRecipe(0); // Delete recipe number 0 in the database. The 0 passed as 
			// argument is the number of the element in the databse, but the number that will be presented to the usrt will be 
			// the number 1.
		assertNotNull(result);
	}

	/**
	 * After there is only one recipe in the book.
	 * When we try to delete a recipe recipe with 
	 * the number higher than the amount of recipes 
	 * -1, the return value of the deleteRecipe method 
	 * must be null
	 * 
	 * 
	 */
	@Test
	public void testDeleteRecipe_NonExistentRecipe_2() {
		coffeeMaker.addRecipe(recipe1); // Add recipe to database. In hte future, this recipe can be accessed with the number 1 in the command prompt and 0 of the database
		String result = coffeeMaker.deleteRecipe(1); // Delete recipe number 1 in the database. The 1 passed as 
		// argument is the number of the element in the databse, but the number that will be presented to the usrt will be 
		// the number 2.
		assertNull(result);
	}

	/**
	 * When the recipe book is empty.
	 * When we try to delete a recipe recipe with 
	 * a negative number, the return value of 
	 * the deleteRecipe method must be null
	 * 
	 * 
	 */
	@Test
	public void testDeleteRecipe_InvalidNegativeRecipeMumber() {
		String result = coffeeMaker.deleteRecipe(-1); // Try to delete a recipe number -1 in the database. The number -1 is out of bounds in the array
		assertNull(result);
	}

	/**
	 * Given a coffee maker with no recipes
	 * When we delete a recipe
	 * Then we get a null response.
	 */
	@Test
	public void testDeleteRecipe_InvalidNonExistentRecipeBookEmpty() {
		String result = coffeeMaker.deleteRecipe(0);
		assertNull(result);
	}

	/**
	 * When the recipe book is empty.
	 * When we try to edit a recipe recipe with 
	 * a negative number, the return value of 
	 * the editRecipe method must be null
	 * 
	 * 
	 */
	@Test
	public void testEditRecipe_NegativeRecipeEmptyBook() {
		String result = coffeeMaker.editRecipe(-1, recipe1); 
		assertNull(result);
	}

	/**
	 * When the recipe book containes one recipe.
	 * When we try to edit a recipe recipe with 
	 * a negative number, the return value of 
	 * the editRecipe method must be null
	 * 
	 * 
	 */
	@Test
	public void testEditRecipe_NegativeRecipeNonEmptyBook() {
		coffeeMaker.addRecipe(recipe1);
		String result = coffeeMaker.editRecipe(-1, recipe1); 
		assertNull(result);
	}

	/**
	 * When the recipe book is empty.
	 * When we try to edit a recipe in the 
	 * position 0 of the book (array counting), 
	 * the return value of the editRecipe method 
	 * must be null
	 */
	@Test
	public void testEditRecipe_NonExistentRecipeNUmber_2() {
		String result = coffeeMaker.editRecipe(0, recipe1); 
		assertNull(result);
	}

	/**
	 * When the recipe book has one recipe
	 * When we try to edit a recipe in the 
	 * position 0 of the book (array counting), 
	 * the return value of the editRecipe method 
	 * must be not null
	 */
	@Test
	public void testEditRecipe_ValidRecipeNumber_nullTest() {
		coffeeMaker.addRecipe(recipe1);
		String result = coffeeMaker.editRecipe(0, recipe2); 
		assertNotNull(result);
		assertEquals(recipe2.getName(), result);
	}

	/**
	 * When the recipe book has one recipe
	 * When we try to edit a recipe in the 
	 * position 0 of the book (array counting), 
	 * the returned name from the editRecipe 
	 * has to be the same has before
	 */
	@Test
	public void testEditRecipe_ValidRecipeNumber_contentTest() {
		coffeeMaker.addRecipe(recipe1);
		String result = coffeeMaker.editRecipe(0, recipe2); 
		assertEquals(recipe1.getName().toString(), result.toString());
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we try to add negative units of a element, 
	 * an inventory exception should be thrown
	 * @throws InventoryException when a negative unit is added
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonValidNegativeCoffee() throws InventoryException {
		coffeeMaker.addInventory("-15", "1", "1", "1");
	}
	
	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we try to add negative units of a element, 
	 * an inventory exception should be thrown
	 * @throws InventoryException when a negative unit is added
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonValidNegativeMilk() throws InventoryException {
		coffeeMaker.addInventory("1", "-15", "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we try to add negative units of a element, 
	 * an inventory exception should be thrown
	 * @throws InventoryException when a negative unit is added
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonValidNegativeSugar() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "-15", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we try to add negative units of a element, 
	 * an inventory exception should be thrown
	 * @throws InventoryException when a negative unit is added
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonValidNegativeChocolate() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "1", "-15");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we try to add negative units of a element, 
	 * an inventory exception should be thrown
	 * @throws InventoryException when a negative unit is added
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonValidNegativeInventory() throws InventoryException {
		coffeeMaker.addInventory("-1", "-1", "-1", "-1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a non numeric string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonNumericStringCoffee() throws InventoryException  {
		coffeeMaker.addInventory("qwe", "1", "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a non numeric string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonNumericStringMilk() throws InventoryException  {
		coffeeMaker.addInventory("1", "qwe", "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a non numeric string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonNumericStringSugar() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "qwe", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a non numeric string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NonNumerictringChocolate() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "1", "qwe");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a null value, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NULLStringCoffee() throws InventoryException  {
		coffeeMaker.addInventory(null, "1", "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a null value, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NULLStringMilk() throws InventoryException  {
		coffeeMaker.addInventory("1", null, "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a null value, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NULLStringSugar() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", null, "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a null value, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_NULLStringChocolate() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "1", null);
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a empty string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_EmptyStringCoffee() throws InventoryException  {
		coffeeMaker.addInventory("", "1", "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a empty string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_EmptyStringMilk() throws InventoryException  {
		coffeeMaker.addInventory("1", "", "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a empty string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_EmptyStringSugar() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When we pass as argument a empty string, 
	 * an inventory exception should be thrown
	 * @throws InventoryException if there was an error parsing the quanity
	* 		to a positive integer.
	 */
	@Test(expected = InventoryException.class)
	public void testAddInventary_EmptyStringChocolate() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "1", "");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When it is passed as argument a positive integer, 
	 * the inventory exception should not the thrown
	 */
	@Test
	public void testAddInventary_ValidInventory() throws InventoryException  {
		coffeeMaker.addInventory("1", "1", "1", "1");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When it is passed as argument a negative integer, 
	 * the inventory exception should not the thrown
	 */
	@Test
	public void testAddInventary_ValidZeroInventory() throws InventoryException {
		coffeeMaker.addInventory("0", "0", "0", "0");
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When it is tryed to add zero to every element 
	 * of the iventory, the inventory should contain 
	 * the same units of every element
	 */
	@Test
	public void testAddInventary_ValidNegativeInventory_CheckInventory_2() throws InventoryException {
		coffeeMaker.addInventory("0", "0", "0", "0");
		String expected = "Coffee: 15\nMilk: 15\nSugar: 15\nChocolate: 15\n";
		String actual = coffeeMaker.checkInventory();
		assertEquals(expected, actual);
	}

	/**
	 * When inventary is in standard mode, contains 
	 * 15 units of every ingredient
	 * When trying to add some positive values, the 
	 * result should be the correct some for each one of the elements.
	 */
	@Test
	public void testAddInventary_ValidNegativeInventory_CheckInventory_3() throws InventoryException {
		coffeeMaker.addInventory("1", "2", "3", "4");
		String expected = "Coffee: 16\nMilk: 17\nSugar: 18\nChocolate: 19\n";
		String actual = coffeeMaker.checkInventory();
		assertEquals(expected, actual);
	}

	/**
	 * When trying to make a coffee and there is 
	 * no recipe in the book, then, it should not 
	 * be possible to make one 
	 * When the user tries to make one coffee without 
	 * one in the book, the return value of the method 
	 * (change) will be the same as the user payed to 
	 * get the coffee
	 */
	@Test
	public void testMakeCofee_NoRecipeInDatabase() {
		int change_expected = 10;
		int change_actual = coffeeMaker.makeCoffee(0, 10);
		assertEquals(change_expected, change_actual);
	}

	/**
	 * When trying to make a coffee and there is 
	 * no recipe in the book, then, it should not 
	 * be possible to make one 
	 * When the user tries to make one coffee without 
	 * one in the book, the return value of the method 
	 * (change) will be the same as the user payed to 
	 * get the coffee.
	 * Besided that, there is not allowed to make a 
	 * coffee with a negative identification number.
	 */
	@Test
	public void testMakeCofee_NegativeRecipeNoRecipeInDatabase() {
		int change_expected = 10;
		int change_actual = coffeeMaker.makeCoffee(-1, 10);
		assertEquals(change_expected, change_actual);
	}

	/**
	 * When the amount the user wants to pay is lower 
	 * than the actual cost of the coffee, the return 
	 * value of the makeCoffee method must be the amout 
	 * the user gave.
	 */
	@Test
	public void testMakeCofee_InvalidRecipeNumber() {
		coffeeMaker.addRecipe(recipe1);
		int change_expected = 10;
		int change_actual = coffeeMaker.makeCoffee(1, 10);
		assertEquals(change_expected, change_actual);
	}

	/**
	 * When the amount the user wants to pay is the 
	 * same as the cost of the coffee, there will be 
	 * no change and the return value must be zero.
	 */
	@Test
	public void testMakeCofee_ValidRecipeNumber() {
		coffeeMaker.addRecipe(recipe1);
		int expected = 0;
		int actual = coffeeMaker.makeCoffee(0, 50);
		assertEquals(expected, actual);
	}

	/**
	 * When trying to make a coffee and there is 
	 * no recipe in the book, then, it should not 
	 * be possible to make one 
	 * When the user tries to make one coffee without 
	 * one in the book, the return value of the method 
	 * (change) will be the same as the user payed to 
	 * get the coffee
	 */
	@Test
	public void testMakeCofee_InvalidPayment() {
		coffeeMaker.addRecipe(recipe1);
		int expected = 20;
		int actual = coffeeMaker.makeCoffee(0, 20);
		assertEquals(expected, actual);
	}

	/**
	 * When the amount the user wants to pay is the 
	 * higher than the cost of the coffee, the change 
	 * will be the difference between both the amout 
	 * payed and the value asked for the coffee.
	 */
	@Test
	public void testMakeCofee_ValidPayment() {
		coffeeMaker.addRecipe(recipe1);
		int expected = 1;
		int actual = coffeeMaker.makeCoffee(0, 51);
		assertEquals(expected, actual);
	}

	/**
	 * When we compare 2 recipes with the same name
	 * Then we get an true response.
	 */
	@Test
	public void testMakeCofee_RecipesWithTheSameNameAreTheSame() {
		recipe2.setName(recipe1.getName());
		assertEquals(true, recipe1.equals(recipe2));
	}


	/**
	 * When we compare 2 recipes with different names
	 * Then we get an false response.
	 */
	@Test
	public void testMakeCofee_RecipesWithDifferentNameAreTheSame() throws RecipeException {
		Recipe new_recipe = new Recipe();
		new_recipe.setName("new_recipe");
		new_recipe.setAmtChocolate( String.valueOf(recipe1.getAmtChocolate()));
		new_recipe.setAmtCoffee(String.valueOf(recipe1.getAmtCoffee()));
		new_recipe.setAmtMilk(String.valueOf(recipe1.getAmtMilk()));
		new_recipe.setAmtSugar(String.valueOf(recipe1.getAmtSugar()));
		new_recipe.setPrice(String.valueOf(recipe1.getPrice()));
		assertEquals(false, recipe1.equals(new_recipe));
	}


}
