package tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

import main.Demo;


public class MainTest {
    // implemented test cases for the main method:
    //  - any of the sides negative: Its not a triangle
    //  - any of the sides is a string: Its not a triangle
    //  - any of the sides 0: Its not a triangle
    //  - side 1 is 5, side 2 is 9, side 3 is 3: Its not a triangle
    //  - side 1 is 5, side 2 is 12, side 3 is 13: Its a triangle
    //  - side 1 is 5, side 2 is 13, side 3 is 12: Its a triangle
    //  - side 1 is 10, side 2 is 10, side 3 is 10: Its a triangle
    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle


    // Any of the sides negative
    @Test
    public void maintestInputNegative() {
        String input = "-1\n2\n3\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());   
    }


    // any of the sides is a string
    @Test
    public void mainTestInputString() {
        String input = "a\nb\nc\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero() {
        String input = "0\n1\n3\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }


    // side 1 is 5, side 2 is 9, side 3 is 3: Its not a triangle
    @Test
    public void mainTestInput593() {
        String input = "5\n9\n3\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

    //  - side 1 is 5, side 2 is 12, side 3 is 13: Its a triangle
    @Test
    public void mainTestInput51213() {
        String input = "5\n12\n13\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }


    //  - side 1 is 5, side 2 is 13, side 3 is 12: Its a triangle
    @Test
    public void mainTestInput51312() {
        String input = "5\n13\n12\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }


    //  - side 1 is 10, side 2 is 10, side 3 is 10: Its a triangle
    @Test
    public void mainTestInput101010() {
        String input = "10\n10\n10\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }



    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle
    @Test
    public void mainTestInput225() {
        String input = "2\n2\n5\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

}
