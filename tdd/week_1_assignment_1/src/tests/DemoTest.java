package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

import main.Demo;


public class DemoTest {
    // implemented test cases for the main method:
    //  - any of the sides negative: Its not a triangle
    //  - any of the sides 0: Its not a triangle
    //  - side 1 is 5, side 2 is 9, side 3 is 3: Its not a triangle
    //  - side 1 is 5, side 2 is 12, side 3 is 13: Its a triangle
    //  - side 1 is 5, side 2 is 13, side 3 is 12: Its a triangle
    //  - side 1 is 10, side 2 is 10, side 3 is 10: Its a triangle
    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle

    // Any of the sides negative
    @Test
    public void maintestInputNegative_1() {
        assertFalse(Demo.isTriangle(-1, 2, 3));   
    }

    // Any of the sides negative
    @Test
    public void maintestInputNegative_2() {
        assertFalse(Demo.isTriangle(1, -2, 3));   
    }

    // Any of the sides negative
    @Test
    public void maintestInputNegative_3() {
        assertFalse(Demo.isTriangle(1, 2, -3));   
    }

    // Any of the sides negative
    @Test
    public void maintestInputNegative_4() {
        assertFalse(Demo.isTriangle(-1, -2, 3));   
    }

    // Any of the sides negative
    @Test
    public void maintestInputNegative_5() {
        assertFalse(Demo.isTriangle(-1, 2, -3));   
    }

    // Any of the sides negative
    @Test
    public void maintestInputNegative_6() {
        assertFalse(Demo.isTriangle(1, -2,-3));   
    }

    // Any of the sides negative
    @Test
    public void maintestInputNegative_7() {
        assertFalse(Demo.isTriangle(-1, -2, -3));   
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero_1() {
        assertFalse(Demo.isTriangle(0, 1, 3));  
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero_2() {
        assertFalse(Demo.isTriangle(1, 0, 3));  
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero_3() {
        assertFalse(Demo.isTriangle(1, 1, 0));  
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero_4() {
        assertFalse(Demo.isTriangle(1, 0, 0));  
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero_5() {
        assertFalse(Demo.isTriangle(0, 1, 0));  
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero_6() {
        assertFalse(Demo.isTriangle(0, 0, 1));  
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero_7() {
        assertFalse(Demo.isTriangle(0, 0, 0));  
    }


    // side 1 is 5, side 2 is 9, side 3 is 3: Its not a triangle
    @Test
    public void mainTestInput593_isTriangle() {
        assertFalse(Demo.isTriangle(5, 9, 3));      
        
    }

    //  - side 1 is 5, side 2 is 12, side 3 is 13: Its a triangle
    @Test
    public void mainTestInput51213_1() {
        assertTrue(Demo.isTriangle(5, 12, 13));       
    }

    //  - side 1 is 5, side 2 is 12, side 3 is 13: Its a triangle
    @Test
    public void mainTestInput51213_2() {
        assertTrue(Demo.isTriangle(13, 5, 12));       
    }

    //  - side 1 is 10, side 2 is 10, side 3 is 10: Its a triangle
    @Test
    public void mainTestInput101010_isTriangle() {
        assertTrue(Demo.isTriangle(10, 10, 10));   
        
    }

    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle
    @Test
    public void mainTestInput225_1() {
        assertFalse(Demo.isTriangle(2, 2, 5));
    }

    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle
    @Test
    public void mainTestInput225_2() {
        assertFalse(Demo.isTriangle(5, 2, 2));
    }

    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle
    @Test
    public void mainTestInput225_3() {
        assertFalse(Demo.isTriangle(2, 5, 2));
    }


    // implemented test cases for the main method:
    //  - any of the sides negative: Its not a triangle
    //  - any of the sides is a string: Its not a triangle
    //  - any of the sides 0: Its not a triangle
    //  - side 1 is 5, side 2 is 9, side 3 is 3: Its not a triangle
    //  - side 1 is 5, side 2 is 12, side 3 is 13: Its a triangle
    //  - side 1 is 5, side 2 is 13, side 3 is 12: Its a triangle
    //  - side 1 is 10, side 2 is 10, side 3 is 10: Its a triangle
    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle
    
    // Any of the sides negative
    @Test
    public void maintestInputNegative() {
        String input = "-1\n2\n3\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());   
    }


    // any of the sides is a string
    @Test
    public void mainTestInputString() {
        String input = "a\nb\nc\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

    //  Any of the sides 0: Its not a triangle
    @Test
    public void mainTestInputZero() {
        String input = "0\n1\n3\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }


    // side 1 is 5, side 2 is 9, side 3 is 3: Its not a triangle
    @Test
    public void mainTestInput593() {
        String input = "5\n9\n3\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

    //  - side 1 is 5, side 2 is 12, side 3 is 13: Its a triangle
    @Test
    public void mainTestInput51213() {
        String input = "5\n12\n13\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }


    //  - side 1 is 5, side 2 is 13, side 3 is 12: Its a triangle
    @Test
    public void mainTestInput51312() {
        String input = "5\n13\n12\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }


    //  - side 1 is 10, side 2 is 10, side 3 is 10: Its a triangle
    @Test
    public void mainTestInput101010() {
        String input = "10\n10\n10\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }



    //  - side 1 is 2, side 2 is 2, side 3 is 5: Its not a triangle
    @Test
    public void mainTestInput225() {
        String input = "2\n2\n5\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter side 1: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 2: " + System.getProperty("line.separator");
        consoleOutput += "Enter side 3: " + System.getProperty("line.separator");
        consoleOutput += "This is not a triangle." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

}
