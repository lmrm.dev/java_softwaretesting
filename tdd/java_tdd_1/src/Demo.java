import java.util.Scanner;
import java.lang.Math;

public class Demo {
    public static void main(String[] args) throws Exception {
        double r, area, circum;
        Scanner reader = new Scanner(System.in);  // Reading from System.in

        // Ask the user for input
        System.out.println("Enter the radius: ");
        try {
            r = reader.nextDouble();
        } catch (java.util.InputMismatchException e) {
            System.out.println("Radius msust be a number");
            reader.close();
            return;
        }
        
        if(r<0){
            System.out.println("Radius must be positive");
            reader.close();
            return;
        }
            

        circum = 2 * Math.PI * r;
        area = Math.PI *  Math.pow(r,2);

        System.out.println("For a circle with radius " + r + ",");
        System.out.println( " the circumference is " + circum);
        System.out.println( " and the area is " + area + ".");

        reader.close();

        
        

    }
}
