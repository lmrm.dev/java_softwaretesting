import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class Demo2Test {

    // implemented test cases:
    //  - radius 1, circum 6.283185307179586, area 3.141592653589793
    //  - radius 3, circum 18.849555921539, area 28.274333882308
    //  - Radius "abc", error "radius msust be a number"
    //  - In case radius is 0
    //  - In case radius is -1, error "radius must be positive"


    @Test
    public void maintestInput1() {
        // Represents user input
        String input = "1\n";

        // data type for system in. Redirect the input from the keyboard to our string
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        // The same general process is true for the output. Obviously we don't know the 
        // output up front, that's not what we're looking for. But, it's the same kind 
        // of ByteArrayOutputStream, so you still make one. You don't pass anything in, 
        // it's just a generic ByteArrayOutputStream. However, what we need to do is make 
        // it a PrintStream. That is the system out setup data type. So we create this 
        // ByteArrayOutputStream as out, and then new PrintStream as the constructor. 
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter the radius: " + System.getProperty("line.separator");
        consoleOutput += "For a circle with radius 1.0," + System.getProperty("line.separator");
        consoleOutput += " the circumference is 6.283185307179586" + System.getProperty("line.separator");
        consoleOutput += " and the area is 3.141592653589793." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

    // radius 3, circum 18.849555921539, area 28.274333882308
    @Test
    public void maintestInput3() {
        // Represents user input
        String input = "3\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter the radius: " + System.getProperty("line.separator");
        consoleOutput += "For a circle with radius 3.0," + System.getProperty("line.separator");
        consoleOutput += " the circumference is 18.84955592153876" + System.getProperty("line.separator");
        consoleOutput += " and the area is 28.274333882308138." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }


    // Radius "ABC", error "radius msust be a number"
    @Test
    public void maintestInputABC() {
        // Represents user input
        String input = "ABC\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            return;
        }

        String consoleOutput = "Enter the radius: " + System.getProperty("line.separator");
        consoleOutput += "Radius msust be a number"+ System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

    // Radius 0, error message
    @Test
    public void maintestInput0() {
        // Represents user input
        String input = "0\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter the radius: " + System.getProperty("line.separator");
        consoleOutput += "For a circle with radius 0.0," + System.getProperty("line.separator");
        consoleOutput += " the circumference is 0.0" + System.getProperty("line.separator");
        consoleOutput += " and the area is 0.0." + System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }

    // Radius -1, error "Radius must be positive"
    @Test
    public void maintestInput_1() {
        // Represents user input
        String input = "-1\n";

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        // Invoke
        String[] args = {};
        try {
            Demo.main(args);
        } catch (Exception e) {
            System.out.println("Exception cought: " + e);
            return;
        }

        // (expected) output
        String consoleOutput = "Enter the radius: " + System.getProperty("line.separator");
        consoleOutput += "Radius must be positive"+ System.getProperty("line.separator");

        assertEquals(consoleOutput, out.toString());        
        
    }
}
